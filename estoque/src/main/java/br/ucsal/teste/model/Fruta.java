package br.ucsal.teste.model;

public class Fruta {

	protected String nome;
	protected Integer quantidade;
	protected Double valorUni;
	protected Double valorTotal;
	
	public Fruta(String nome) {
		super();
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public Double getValorUni() {
		return valorUni;
	}
	public void setValorUni(Double valorUni) {
		this.valorUni = valorUni;
	}
	public Double getValorTotal() {
		if (this.nome.equalsIgnoreCase("jaca")) {
			 this.valorTotal = this.valorUni * Double.valueOf(this.quantidade);
			 return valorTotal;
		}
		return valorTotal;
	}
}
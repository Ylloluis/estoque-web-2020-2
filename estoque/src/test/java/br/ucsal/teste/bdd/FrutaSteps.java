package br.ucsal.teste.bdd;

import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrutaSteps {
    WebDriver chromeDriver;

    WebElement produto;
    WebElement quantidadeFruta;
    WebElement valorFruta;
    WebElement botao;

    private final static String URL = "file:///home/ylloluis/IdeaProjects/estoque-web-2020-2/estoque/src/main/webapp/lista-compras.html";



    @Given("estou na lista de compras")
    public void givenEstouNaListaDeCompras() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
        chromeDriver = new ChromeDriver();
        chromeDriver.get(URL);
    }

    @When("seleciono o produto $produto")
    public void whenSelecionoOProdutoManga(String nome) {
        produto = chromeDriver.findElement(By.id("listaProdutos"));
        produto.sendKeys(nome);
    }
    @When("informo a quantidade $qtd")
    public void whenInformoAQuantidade(String quantidade) {
        quantidadeFruta = chromeDriver.findElement(By.id("quantidade"));
        quantidadeFruta.sendKeys(quantidade);
    }

    @When("informo o valor unit\u00E1rio $valor reais")
    public void whenInformoOValorUnitário(String valor) {
        valorFruta = chromeDriver.findElement(By.id("valorUnitario"));
        valorFruta.sendKeys(valor);
    }

    @When("confirmo a compra")
    public void whenConfirmoACompra() {
        JavascriptExecutor js = (JavascriptExecutor) chromeDriver;
        botao = chromeDriver.findElement(By.id("calcularBtn"));
        botao.click();
    }

    @Then("terei de pagar $valor reais")
    public void thenTereiDePagar(String valor) {
        JavascriptExecutor js = (JavascriptExecutor) chromeDriver;
        String soma = js.executeScript("return document.getElementById('valorTotal').value").toString();
        Assert.assertEquals(valor, soma);
    }

    @AfterScenario
    public void destroy() {
        chromeDriver.quit();
    }
}

package br.ucsal.teste.unitario;

import br.ucsal.teste.model.Fruta;
import org.junit.*;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;


public class MainTest {
	
    static private WebDriver chromeDriver;
    static final private String URL = "file:///home/ylloluis/IdeaProjects/estoque-web-2020-2/estoque/src/main/webapp/lista-compras.html";

    @Before
    public void prepare() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
        chromeDriver = new ChromeDriver();
    }


    @Test
    public void mainTest() throws InterruptedException {
        chromeDriver.get(URL);
        WebElement quantidade = chromeDriver.findElement(By.id("quantidade"));
        WebElement valor = chromeDriver.findElement(By.id("valorUnitario"));
        WebElement botao = chromeDriver.findElement(By.id("calcularBtn"));

        Integer qnt = 5;
        Integer valorUnit = 5;

        quantidade.sendKeys(qnt.toString());
        valor.sendKeys(valorUnit.toString());
        botao.click();

        Thread.sleep(200);

        JavascriptExecutor js = (JavascriptExecutor) chromeDriver;

        String soma = js.executeScript("return document.getElementById('valorTotal').value").toString();

        if(soma.equals(null)) {
            Assert.fail();
        } else {
            Assert.assertEquals(Integer.toString(25),soma);
        }

    }

    @After
    public void destroy() {
        chromeDriver.quit();
    }
    

}